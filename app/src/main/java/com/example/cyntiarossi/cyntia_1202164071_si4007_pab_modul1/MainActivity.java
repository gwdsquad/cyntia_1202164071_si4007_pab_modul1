package com.example.cyntiarossi.cyntia_1202164071_si4007_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText atas, bawah;
    Button cek;
    TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        atas = (EditText) findViewById(R.id.atas);
        bawah = (EditText) findViewById(R.id.bawah);
        cek = (Button) findViewById(R.id.cek);
        hasil = (TextView) findViewById(R.id.hasil);

        cek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer atas1 = Integer.parseInt(atas.getText().toString());
                Integer bawah1 = Integer.parseInt(bawah.getText().toString());
                Integer hasil1 = atas1*bawah1;
                hasil.setText(String.valueOf(hasil1));

            }
        });
    }
}
